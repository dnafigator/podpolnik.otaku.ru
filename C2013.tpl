<div id="logo">
<div id="logotext">
	<img src="/img/logo2012.jpg?a" />

<div id="titlesign">
<p>Дорогие друзья!<br/>
Мы рады представить вам самое тихое мероприятие года</p>
<h1>Негромкий подпольник 2013</h1>
</div>
</div>
</div>

<div id="announce">
<div id="content">

<h2>Благодарности</h2>
<p>
Подпольник благополучно прошел, туда ему и&nbsp;дорога.<br/>
Оргкомитет сердечно благодарит всех причастных.<br/>
<!--ul>
<li>за&nbsp;оборудование, договор с&nbsp;баром, работу с&nbsp;аппаратурой&nbsp;&mdash; Сосед, 2net и&nbsp;их&nbsp;команда&nbsp;&mdash; и&nbsp;заодно за&nbsp;последующую ночную опен-эйр пати!</li>
<li>Евгений Щукин и&nbsp;Дмитрий Бобонов&nbsp;&mdash; за&nbsp;организацию тира!</li>
<li>Евгений Путилин&nbsp;&mdash; за&nbsp;организацию игры в&nbsp;го!</li>
<li>Ню!, Пауль, Паша, Мисато и&nbsp;Сателла&nbsp;&mdash; за&nbsp;организацию внезапно-дефиле-люкса!</li>
<li>Мисато&nbsp;&mdash; за&nbsp;драки на&nbsp;лайтсейберах!</li>
<li>Кицунеко&nbsp;&mdash; за&nbsp;конкурсы!</li>
<li>Фирму &laquo;Реанимедиа&raquo;&nbsp;&mdash; за&nbsp;призы!</li>
<li>Кохаку и&nbsp;Некошь&nbsp;&mdash; за&nbsp;маскот!</li>
<li>Участников&nbsp;&mdash; за преодоление лени!</li>
<li>Зрителей&nbsp;&mdash; за то, что продолжаете радовать нас своим присутствием!</li>
</ul-->
С&nbsp;вами была команда оргов и&nbsp;ведущих&nbsp;&mdash; dNafigator, Веня, Мотоко и&nbsp;Астис.<br/>
Оставайтесь с&nbsp;нами!<br/>
</p>

<h2>Фоты!</h2>
<p>
Ссылки на галереи ВК:
<ul>
<li><a href="https://vk.com/album2387912_180576481">https://vk.com/album2387912_180576481</a>
</ul>
Все фотографии взяты из открытых источников и принаждлежат своим владельцам.
</p>

<h2>Поделиться</h2>
<p>Если у вас хватило терпения дочитать до этого места — немедленно поделитесь с друзьями
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style "

addthis:title="А ты идешь на «Негромкий подпольник»?"
addthis:url="http://podpolnik.otaku.ru/"
>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_facebook"></a>
<a class="addthis_button_vk"></a>
<a class="addthis_button_livejournal"></a>
<a class="addthis_button_email"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<!--div id="imagealign">
<img src="img/title2011a.gif" />
</div-->

<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e2727a27fdc7c89"></script>
<!-- AddThis Button END -->
</p>

</div>
</div>
