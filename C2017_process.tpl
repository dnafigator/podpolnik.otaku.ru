<div id="logo">
<div id="logotext">
	<img src="/img/logo2017.jpg" />

<div id="titlesign">
<p>Дорогие друзья!<br/>
Мы рады представить вам самое лесное мероприятие года</p>
<h1>Воронежский подпольник 2017</h1>
</div>
</div>
</div>

<div id="announce">
    <div id="content">

            <p>Как выбрать летом выходные, в&nbsp;которые не&nbsp;происходит других фестивалей?</p>
            <p>Да&nbsp;практически невозможно. Поэтому <b>2&nbsp;сентября,</b> когда осень вроде&nbsp;бы началась, но&nbsp;не&nbsp;по-настоящему, когда одни косплееры вернутся с&nbsp;торжественной линейки, а&nbsp;другие заберут оттуда своих детей, мы&nbsp;все снова поедем в&nbsp;лес!</p>
            <p>&laquo;Подпольник-2017&raquo; ждет вас, дорогие друзья! Нас немного, и&nbsp;мы&nbsp;рады <b>каждому участнику:</b> и&nbsp;тем, кто только набирается смелости выйти на&nbsp;сцену, и&nbsp;мэтрам фестиваля, которые еще не&nbsp;настолько заросли мхом серьезности, чтобы отказать себе в&nbsp;возможности повеселиться.
            <p>Кроме того, на&nbsp;Подпольнике традиционно присутствуют <b>координаторы косплея Воронежского фестиваля</b>, которые не&nbsp;только увидят вас на&nbsp;сцене, но&nbsp;и&nbsp;могут в&nbsp;непринужденной обстановке (а&nbsp;не&nbsp;в&nbsp;беготне предвальтельной или генеральной репетиций к&nbsp;фестивалю) дать <b>объективный отзыв</b> вашему выступлению и&nbsp;посоветовать, над чем надо работать, чтобы попасть на&nbsp;настоящую большую сцену на&nbsp;майские праздники.
            <p>Подпольник&nbsp;&mdash; это отличная возможность и&nbsp;для тех, кто хочет устроить <b>развлечение на&nbsp;Форум-выставке</b>, но&nbsp;еще сомневается в&nbsp;своих силах. Ввиду почти полного отсутствия конкуренции вы&nbsp;можете протестировать все ваши идеи в&nbsp;рабочем режиме!
            <p>Итак, что нужно делать:
            <ul>
            <li>подавать <b><a href="http://podpolnik.cosplay2.ru/create_request" target="_blank">заявки</a> на&nbsp;песни, танцы, сценки</b> и&nbsp;что еще угодно в&nbsp;выступательную часть;</li>
            <li>подавать <b><a href="http://podpolnik.cosplay2.ru/create_request" target="_blank">заявки</a> на&nbsp;мини-ярмарку</b>, если вы&nbsp;хотите чем-то торговать (это бесплатно, но&nbsp;столов не&nbsp;дадим);</li>
            <li>подавать <b><a href="http://podpolnik.cosplay2.ru/create_request" target="_blank">заявки</a> на&nbsp;развлечение зрителей</b> (или просто подать заявку на&nbsp;помощника организатора или волонтера, если вы&nbsp;хотите нам помочь, но&nbsp;не&nbsp;знаете, чем: у&nbsp;нас вечно не&nbsp;хватает рук!).</li>
            </ul>
            И, конечно, придти <b>2&nbsp;сентября</b> <b><a href="/map2017">на&nbsp;поляну</a></b> <b>с&nbsp;14:00</b> и&nbsp;повеселиться вместе с&nbsp;нами!
            <p>Косплей приветствуется: традиционный конкурс зрительских костюмов ждет вас.
			<p>Не забудьте вступить в <b><a target="_blank" href="https://vk.com/podpolnik">нашу группу</a></b>, чтобы не пропустить ничего важного.</p>

            <div><img src="img/logo2017down.png" /></div>

			<p><b><a href="http://podpolnik.cosplay2.ru/create_request" target="_blank">Подать заявку</a></b></p>
			<p><b><a href="https://vk.com/vrnpodpolnik2017" target="_blank">Наша встреча в VK</a></b></p>

        <h2>Поделиться</h2>
        <p>Если у вас хватило терпения дочитать до этого места — немедленно поделитесь с друзьями
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style " addthis:title="А ты идешь на «Негромкий подпольник»?" addthis:url="http://podpolnik.otaku.ru/">
            <a class="addthis_button_twitter"></a>
            <a class="addthis_button_facebook"></a>
            <a class="addthis_button_vk"></a>
            <a class="addthis_button_livejournal"></a>
            <a class="addthis_button_email"></a>
            <a class="addthis_button_compact"></a>
            <a class="addthis_counter addthis_bubble_style"></a>
        </div>
        <script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e2727a27fdc7c89"></script>
        <!-- AddThis Button END -->
        </p>

    </div>
</div>
