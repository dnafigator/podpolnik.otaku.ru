<div id="logo">
<div id="logotext">
	<img src="/img/logo2017.jpg" />

<div id="titlesign">
<p>Дорогие друзья!<br/>
Мы рады представить вам самое тихое мероприятие года</p>
<h1>Негромкий подпольник 2017</h1>
</div>
</div>
</div>

<div id="announce">
<div id="content">

<h2>Благодарности</h2>
<p>
Хотим сказать большое спасибо всем участникам, зрителям, волонтерам, всем-всем кто помогал, подавал заявки, ехал из других городов ради Подпольника, слушал, смотрел и провел чудесные выходные с нами!)<br>
Ждем вас в следующем году!
</p>

<!--h2>Фоты!</h2>
<p>
Мы тут немного фоток нашли:
<ul>
</ul>
Все фотографии взяты из открытых источников и принаждлежат своим владельцам.
</p-->

<h2>Поделиться</h2>
<p>Если у вас хватило терпения дочитать до этого места — немедленно поделитесь с друзьями
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style "

addthis:title="А ты идешь на «Негромкий подпольник»?"
addthis:url="http://podpolnik.otaku.ru/"
>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_facebook"></a>
<a class="addthis_button_vk"></a>
<a class="addthis_button_livejournal"></a>
<a class="addthis_button_email"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<!--div id="imagealign">
<img src="img/title2011a.gif" />
</div-->

<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e2727a27fdc7c89"></script>
<!-- AddThis Button END -->
</p>

</div>
</div>