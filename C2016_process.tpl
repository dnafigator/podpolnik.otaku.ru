<div id="logo">
<div id="logotext">
	<img src="/img/logo2016.jpg" />

<div id="titlesign">
<p>Дорогие друзья!<br/>
Мы рады представить вам самое тихое мероприятие года</p>
<h1>Негромкий подпольник 2016</h1>
</div>
</div>
</div>

<div id="announce">
    <div id="content">

		<h2>Друзья!</h2>
			<p>Мы никак не успокоимся: вот снова устроили <b>Подпольник</b>!</p>
			<p>Он пройдет <b>10 сентября</b> (это суббота и НЕ День города), все тогда же (<b>с 14:00</b>). Традиционно у нас можно посмотреть (и показать!) <b>любые</b> номера, особенно те, что не прошли на крупные фестивали или просто не дождались их.</p>
			<p>Многие выступавшие на Подпольнике утверждают, что получили очень необычные и интересные ощущения: под ногами не пол сцены, а трава, вместо кулис &ndash; березки, вместо аудитории в 2000 зрителей &ndash; уютная компания.</p>
			<p>Присоединяйтесь и вы! Хотите<b> петь, танцевать, рассказывать стихи, проводить мастер-классы, устраивать развлечения для гостей мероприятия?</b> Отлично, подавайте <a href="http://podpolnik.cosplay2.ru/create_request"><b>заявку</b></a>!</p>
			<p>Хотите сами быть <b>гостем</b>, развлекаться и смотреть на тех, кто поет, танцует и так далее? Просто <b>приезжайте</b> к нам! Вход <b>бесплатный</b>, косплей <b>необязателен</b> (всегда можно сказать, что вы косплеите сами себя).</p>
			<p>Не забудьте вступить в<b> <a href="https://vk.com/vrnpodpolnik2016">нашу группу</a></b>, чтобы не пропустить ничего важного.</p>
			<p><b><a href="http://podpolnik.cosplay2.ru/create_request" target="_blank">Подать заявку</a></b></p>
			<p><b><a href="https://vk.com/vrnpodpolnik2016" target="_blank">Наша встреча в VK</a></b></p>

        <h2>Поделиться</h2>
        <p>Если у вас хватило терпения дочитать до этого места — немедленно поделитесь с друзьями
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style " addthis:title="А ты идешь на «Негромкий подпольник»?" addthis:url="http://podpolnik.otaku.ru/">
            <a class="addthis_button_twitter"></a>
            <a class="addthis_button_facebook"></a>
            <a class="addthis_button_vk"></a>
            <a class="addthis_button_livejournal"></a>
            <a class="addthis_button_email"></a>
            <a class="addthis_button_compact"></a>
            <a class="addthis_counter addthis_bubble_style"></a>
        </div>
        <!--div id="imagealign">
        <img src="img/title2011a.gif" />
        </div-->

        <script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e2727a27fdc7c89"></script>
        <!-- AddThis Button END -->
        </p>

    </div>
</div>
