<div id="logo">
<div id="logotext">
	<img src="/img/logo2012.jpg?a" />

<div id="titlesign">
<p>Дорогие друзья!<br/>
Мы рады представить вам самое тихое мероприятие года</p>
<h1>Негромкий подпольник 2012</h1>
</div>
</div>
</div>

<div id="announce">
<div id="content">

<h2>Где</h2>

<a href="?y=map2012.tpl">В лесу недалеко от Воронежа</a> 18 августа 2012 (суббота)
в 14:00 — общий сбор на остановке &laquo;Студгородок ВПИ&raquo; (9-й километр).
<!--Вы даже можете добавить это событие себе <a href="http://www.google.com/calendar/event?action=TEMPLATE&text=%D0%9D%D0%B5%D0%B3%D1%80%D0%BE%D0%BC%D0%BA%D0%B8%D0%B9%20%D0%BF%D0%BE%D0%B4%D0%BF%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%202011&dates=20110827T100000Z/20110827T190000Z&details=&location=%D0%92%D0%BE%D1%80%D0%BE%D0%BD%D0%B5%D0%B6%2C%209%20%D0%BA%D0%B8%D0%BB%D0%BE%D0%BC%D0%B5%D1%82%D1%80%2C%20%D0%BB%D0%B5%D1%81&trp=true&sprop=http%3A%2F%2Fpodpolnik.otaku.ru%2F&sprop=name:%D0%9D%D0%B5%D0%B3%D1%80%D0%BE%D0%BC%D0%BA%D0%B8%D0%B9%20%D0%BF%D0%BE%D0%B4%D0%BF%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%202011" target="_blank">в&nbsp;календарь</a>, или присоединиться к нему <a target="_blank" href="http://vkontakte.ru/event28902966">ВКонтакте</a>.-->
</p>

<h2>Что</h2>
<p>18 августа на поляну совершит незапланированную посадку неведомая космическая летакля серебристого металла.</p>
<p>Стар Трек, дорогие мои друзья, Стар Трек отдается нам на поругание на нашем ежегодном собрании плащей из занавесок и пирамидхедов из подушки!
И тема нашего пикника – БУНТ НА КОРАБЛЕ!
Запретили ковыряться в носу, шествие на фестивале и возвращаться домой после полуночи? Давайте же уберем бластеры в кобуру и это мирно обсудим.</p>

<h2>Участникам</h2>
<p>Мы ждем от вас любых представлений, на тему и без нее, хоть вообще про совсем-совсем другое, но главное – мы вас ждем! А тема – ну так, для вдохновения, вдруг кому-то поможет.</p>
<p>Подайте <a href="/app">заявку</a>!</p>

<h2>Активным и небезразличным</h2>
<p>Кто хочет заниматься развлечениями аборигенов данной планеты – тиром, боями на подушках, _____ (вписать нужное), или кто хочет помочь в этом занятии – обращайтесь <a href="mailto:podpolnik@otaku.ru">на капитанский мостик</a>!</p>

<h2>Важно!</h2>
<p>В этот раз у нас будет звук! Да-да, настоящие колонки и микрофон (или два).</p>

<h2>Внезапно!</h2
<p>В этом году у нас будет Внезапно-Дефиле-Люкс!<br>
Члены оргкомитета Дефиле-Люкс Танибаты и Воронежского фестиваля, Ню!, Пауль, Мисато, Паша и Сателла, любезно согласились поучаствовать в Подпольнике!<br>

Итак, Внезапно-Дефиле-Люкс!<br>

Что же нужно сделать, чтобы показать себя зрителям и оргам Дефиле-Люкса:<br>
Нужно быть на месте проведения Подпольника завтра (в субботу, в день Подпольника) в 13:00! С костюмами.<br>

ВНИМАНИЕ! ВСЕМ УЧАСТНИКАМ тоже следует там быть в 13:00!
</p>

<h2>Связаться с организаторами</h2>
<p>Отправляйте ваши идеи, мысли, вопросы и даже претензии на <a href="mailto:podpolnik@otaku.ru">podpolnik@otaku.ru</a></p>

<p><a href="https://twitter.com/podpolnik_vrn" class="twitter-follow-button" data-show-count="false" data-lang="ru">Читать @podpolnik_vrn</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

</p>


<h2>Ночное опен-эйр-пати</h2>
<img src="img/logoloud2012.jpg" 
alt="В этом году в рамках Негромкого Подпольника представляем вам Громкое Ночное Опен-Эйр-Пати от людей, которые знают толк в аниме-пати. Вас ждёт незабываемая ночь в лесу. Ведущие, АМВ на открытом воздухе и самое главное - это мощная дискотека с диджеем (10 кВт звука). Такое вы нигде ещё не слышали. Так что берите с собой палатки и оставайтесь до утра."
/>


<h2>Поесть</h2>
На Подпольнике для вас будет работать БАР. Еда и холодные напитки. Настоящий пикник!

<h2>Поделиться</h2>
<p>Если у вас хватило терпения дочитать до этого места — немедленно поделитесь с друзьями
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style "

addthis:title="А ты идешь на «Негромкий подпольник»?"
addthis:url="http://podpolnik.otaku.ru/"
>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_facebook"></a>
<a class="addthis_button_vk"></a>
<a class="addthis_button_livejournal"></a>
<a class="addthis_button_email"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<!--div id="imagealign">
<img src="img/title2011a.gif" />
</div-->

<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e2727a27fdc7c89"></script>
<!-- AddThis Button END -->
</p>

</div>
</div>
