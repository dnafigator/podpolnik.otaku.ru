<?php
include ('config.php');
$dblocation = DB_HOST;
$dbname = DB_NAME ;
$dbuser = DB_LOGIN;
$dbpasswd = DB_PASS;

    $dbcon = @mysql_connect($dblocation,$dbuser,$dbpasswd);

    if (!$dbcon) {       
        echo( "<P>В настоящий момент сервер базы данных недоступен, поэтому корректное отображение страницы невозможно.</P>" );
        exit();
    }
    if (!@mysql_select_db($dbname, $dbcon)) {          
        echo( "<P>В настоящий момент база данных не доступна, поэтому корректное отображение страницы невозможно.</P>" );
        exit();
    }
mysql_query('SET NAMES utf8;');

function slashes(&$el) {
    if (is_array($el))
        foreach($el as $k=>$v)
            slashes($el[$k]);
    else $el = stripslashes($el);
}

if (ini_get('magic_quotes_gpc')) {
    slashes($_GET);
    slashes($_POST);
    slashes($_COOKIE);
}