<div id="logo">
<div id="logotext">
	<img src="img/logo2011b.jpg" />
	<div id="titlesign">
		<p>Дорогие друзья!<br/>
		Мы рады представить вам самое тихое мероприятие года</p>
		<h1>Негромкий подпольник 2011</h1>
	</div>
</div>
</div>

<div id="announce">
<div id="content">

<!--Подпольник окончен. Всем спасибо. Скоро здесь будут фотографии.-->
<div class="ad-gallery" id="photos">
  <div class="ad-image-wrapper">
  </div>
  <div class="ad-nav">
    <div class="ad-thumbs">
      <ul class="ad-thumb-list">
        <li>
          <a href="img/2011/01.jpg">
            <img src="img/2011/01s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/02.jpg">
            <img src="img/2011/02s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/03.jpg">
            <img src="img/2011/03s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/04.jpg">
            <img src="img/2011/04s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/05.jpg">
            <img src="img/2011/05s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/06.jpg">
            <img src="img/2011/06s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/07.jpg">
            <img src="img/2011/07s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/08.jpg">
            <img src="img/2011/08s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/09.jpg">
            <img src="img/2011/09s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/10.jpg">
            <img src="img/2011/10s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/11.jpg">
            <img src="img/2011/11s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/12.jpg">
            <img src="img/2011/12s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/13.jpg">
            <img src="img/2011/13s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/14.jpg">
            <img src="img/2011/14s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/15.jpg">
            <img src="img/2011/15s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/16.jpg">
            <img src="img/2011/16s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/17.jpg">
            <img src="img/2011/17s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/18.jpg">
            <img src="img/2011/18s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/19.jpg">
            <img src="img/2011/19s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/20.jpg">
            <img src="img/2011/20s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/21.jpg">
            <img src="img/2011/21s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/22.jpg">
            <img src="img/2011/22s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/23.jpg">
            <img src="img/2011/23s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/24.jpg">
            <img src="img/2011/24s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/25.jpg">
            <img src="img/2011/25s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/26.jpg">
            <img src="img/2011/26s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/27.jpg">
            <img src="img/2011/27s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/28.jpg">
            <img src="img/2011/28s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/29.jpg">
            <img src="img/2011/29s.jpg" />
          </a>
        </li>
        <li>
          <a href="img/2011/30.jpg">
            <img src="img/2011/30s.jpg" />
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>
<br/>Фотограф &mdash; <a target="_blank" href="http://vk.com/tarasvb">Тарас Бутейко</a>
</div>
</div>