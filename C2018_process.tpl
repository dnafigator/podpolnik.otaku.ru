<div id="logo">
<div id="logotext">
	<img src="/img/logo2018.jpg?2" />

<div id="titlesign">
<p>Дорогие друзья!<br/>
Мы рады представить вам самое рукодельное мероприятие года</p>
<h1>Хенд-мейд Подпольник-2018</h1>
</div>
</div>
</div>

<div id="announce">
    <div id="content">
        <h2>Добро пожаловать!</h2>
<p>Как обычно, программа почти полностью состоит из&nbsp;ВАС, дорогие гости. Во-первых, в&nbsp;этот раз мы&nbsp;хотим предоставить по&nbsp;минуте славы для всех, кто занимается косплейным рукоделием (крафтом, париками итд)&nbsp;&mdash; порекламируйте себя со&nbsp;сцены, и&nbsp;новые заказчики как набегут на&nbsp;вас! (Для этого надо будет подать <b><a href="http://podpolnik2018.cosplay2.ru/create_request" target="_blank">заявку</a></b>&nbsp;&mdash; и&nbsp;мы&nbsp;рекомендуем запастить собственными визитками для раздачи). Конечно, вы&nbsp;можете и&nbsp;торговать своей продукцией (не&nbsp;забудьте столик или скатерть).</p>
<p>Во-вторых, конечно&nbsp;же, мы&nbsp;принимаем <b><a href="http://podpolnik2018.cosplay2.ru/create_request" target="_blank">заявки</a></b> на&nbsp;выступления! Вокал, показательные бои, сценки, танцы&nbsp;&mdash; все, что угодно! Кстати, среди зрителей будут организаторы Воронежского фестиваля, которые смогут оценить вас заранее...</p>
<p>Не&nbsp;забывайте приходить в&nbsp;костюмах, ведь всех желающих ждет призовое Дефиле-Лес от&nbsp;организаторов Дефиле-Люкса! Для этого подавать заявку не&nbsp;надо.</p>
<p>Хотите что-то делать на&nbsp;Подпольнике, но&nbsp;не&nbsp;нашли себя в&nbsp;перечисленных пунктах? Не&nbsp;беда, мы&nbsp;принимаем любые <b><a href="http://podpolnik2018.cosplay2.ru/create_request" target="_blank">заявки</a></b> на&nbsp;проведение развлечений в&nbsp;лесу. Кроме того, наши традиционные активности (бои на&nbsp;палках, фото и&nbsp;артсушка) всегда готовы принять помощь в&nbsp;организации себя.</p>
			<p><b><a href="http://podpolnik2018.cosplay2.ru/create_request" target="_blank">Подать заявку</a></b></p>
			<p><b><a href="https://vk.com/podpolnik2018" target="_blank">Наша встреча в VK</a></b></p>

        <h2>Как добраться</h2>
        <p>Посмотрите на <a href="https://www.google.com/maps/d/viewer?dg=feature&msa=0&mid=1uq0YFhM8tnmMJffBINd1cJaKjb0&ll=51.74865600040091%2C39.16294650000009&z=14" target="_blank">карту!</a></p>
        <h2>Поделиться</h2>
        <p>Если у вас хватило терпения дочитать до этого места — немедленно поделитесь с друзьями
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style " addthis:title="А ты идешь на «Негромкий подпольник»?" addthis:url="http://podpolnik.otaku.ru/">
            <a class="addthis_button_twitter"></a>
            <a class="addthis_button_facebook"></a>
            <a class="addthis_button_vk"></a>
            <a class="addthis_button_livejournal"></a>
            <a class="addthis_button_email"></a>
            <a class="addthis_button_compact"></a>
            <a class="addthis_counter addthis_bubble_style"></a>
        </div>
        <script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e2727a27fdc7c89"></script>
        <!-- AddThis Button END -->
        </p>

    </div>
</div>
