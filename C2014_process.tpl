<div id="logo">
<div id="logotext">
	<img src="/img/logo2014.jpg?b" />

<div id="titlesign">
<p>Дорогие друзья!<br/>
Мы рады представить вам самое тихое мероприятие года</p>
<h1>Негромкий подпольник 2014</h1>
</div>
</div>
</div>

<div id="announce">
<div id="content">

<h2>Заранее</h2>
<p>По просьбам потенциальных участников, открыт <a href="/app">приём заявок</a> на Негромкий 2014.</p>
<p>Мы уже и с датой определилсь, это будет <a target="_blank" href="https://www.google.com/calendar/event?action=TEMPLATE&text=Негромкий+подпольник+2014&dates=20140830T060000Z/20140830T150000Z&details=http://podpolnik.otaku.ru/">30 августа 2014</a>. Место проведения &mdash; уже привычная вам поляна на 9-м километре, вот <a target="_blank" href="/?y=map2014">схема проезда</a>.</p>
<img src="img/forest.jpg">
<p>Вы все, конечно, помните, что у нас есть <a target="_blank" href="http://vk.com/podpolnik">группа</a> и <a target="_blank" href="http://vk.com/podpolnik2014">событие</a> вКонтакте!</p>

<p>
С&nbsp;вами будет команда оргов и&nbsp;ведущих&nbsp;&mdash; Астис, Веня, Мотоко и&nbsp;dNafigator.<br/>
Оставайтесь с&nbsp;нами!<br/>
</p>


<h2>А что потом?</h2><br/>
<img src="img/logoloud2014.jpg" 
alt="И в этом году люди, которые знают толк в аниме-пати, проведут для вас Громкое Ночное Опен-Эйр-Пати в рамках (и немного за рамками) Негромкого Подпольника. Вас ждёт незабываемая ночь в лесу. Так что берите с собой палатки и оставайтесь до утра."
/><br/>
<p>И в этом году люди, которые знают толк в аниме-пати, проведут для вас Громкое Ночное Опен-Эйр-Пати в рамках (и немного за рамками) Негромкого Подпольника. Вас ждёт незабываемая ночь в лесу. Так что берите с собой палатки и оставайтесь до утра.</p>
<p><a target="_blank" href="https://vk.com/event73393686">Событие</a> вКонтакте.</p>

<!--h2>Фоты!</h2>
<p>
Ссылки на галереи ВК:
<ul>
<li><a href="http://vk.com/album57687220_160813407">http://vk.com/album57687220_160813407</a>
<li><a href="http://vk.com/album95508504_161515241">http://vk.com/album95508504_161515241</a></li>
<li><a href="http://vk.com/album74996007_161400629">http://vk.com/album74996007_161400629</a></li>
<li><a href="http://vk.com/album35869646_161199673">http://vk.com/album35869646_161199673</a></li>
<li><a href="http://vk.com/album11429085_161439742">http://vk.com/album11429085_161439742</a></li>
<li><a href="http://vk.com/album-40980147_161487690">http://vk.com/album-40980147_161487690</a></li>
</ul>
Все фотографии взяты из открытых источников и принаждлежат своим владельцам.
</p-->

<h2>Поделиться</h2>
<p>Если у вас хватило терпения дочитать до этого места — немедленно поделитесь с друзьями
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style "

addthis:title="А ты идешь на «Негромкий подпольник»?"
addthis:url="http://podpolnik.otaku.ru/"
>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_facebook"></a>
<a class="addthis_button_vk"></a>
<a class="addthis_button_livejournal"></a>
<a class="addthis_button_email"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<!--div id="imagealign">
<img src="img/title2011a.gif" />
</div-->

<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e2727a27fdc7c89"></script>
<!-- AddThis Button END -->
</p>

</div>
</div>
