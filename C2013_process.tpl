<div id="logo">
<div id="logotext">
	<img src="img/logo2012.jpg?a" />

<div id="titlesign">
<p>Дорогие друзья!<br/>
Мы рады представить вам самое тихое мероприятие года</p>
<h1>Негромкий подпольник 2013</h1>
<a target="_blank" href="http://podpolnik.otaku.ru/app">Подать заявку</a>
&nbsp;
<a target="_blank" href="https://www.google.ru/maps/ms?msid=213639761844676313862.0004c7658c31a11630bb3&msa=0&ll=51.757269,39.180593&spn=0.044097,0.077162">Карта проезда</a>
</div>
</div>
</div>

<div id="announce">
<div id="content">

<h2>Подпольник</h2>
<p>
Сбор &ndash; в 14:00 24 августа (суббота) на остановке «Студгородок ВПИ» (9-й километр).<br>
Место проведения &ndash; фестивальная поляна за Семилукским лесхозом.
</p>

<h2>Вопросы? Звонки? Перипетии?</h2>
<p>
Жизнь происходит в тематическом <a target="_blank" href="http://vk.com/vrnpodpolnik2013">событии вКонтакте</a>.<br>
А если вы уже решились, то можете <a target="_blank" href="http://podpolnik.otaku.ru/app">подать заявку</a>.
</p>

<h2>Ночное опен-эйр-пати</h2><br/>
<img src="img/logoloud2013.jpg" 
alt="В этом году в рамках Негромкого Подпольника представляем вам Громкое Ночное Опен-Эйр-Пати от людей, которые знают толк в аниме-пати. Вас ждёт незабываемая ночь в лесу. Так что берите с собой палатки и оставайтесь до утра."
/><br/>
Событие <a target="_blank" href="https://vk.com/event56795842">вКонтакте</a>.

<h2>Поделиться</h2>
<p>Если у вас хватило терпения дочитать до этого места — немедленно поделитесь с друзьями
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style "

addthis:title="А ты идешь на «Негромкий подпольник»?"
addthis:url="http://podpolnik.otaku.ru/"
>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_facebook"></a>
<a class="addthis_button_vk"></a>
<a class="addthis_button_livejournal"></a>
<a class="addthis_button_email"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<!--div id="imagealign">
<img src="img/title2011a.gif" />
</div-->

<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e2727a27fdc7c89"></script>
<!-- AddThis Button END -->
</p>

</div>
</div>
