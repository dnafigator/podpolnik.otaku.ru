<div id="logo">
<div id="logotext">
	<img src="/img/logo2015.jpg?a" />

<div id="titlesign">
<p>Дорогие друзья!<br/>
Мы рады представить вам самое тихое мероприятие года</p>
<h1>Негромкий подпольник 2015</h1>
</div>
</div>
</div>

<div id="announce">
<div id="content">

<h2>Благодарности</h2>
<p>
Подпольник прошел, спасибо всем!<br/>
Приходите в следующем году!<br/><br/>
С&nbsp;вами была команда оргов и&nbsp;ведущих&nbsp;&mdash; dNafigator, Веня, Мотоко и&nbsp;Астис.<br/>
Оставайтесь с&nbsp;нами!<br/>
</p>

<h2>Фоты!</h2>
<p>
Мы тут немного фоток нашли:
<ul>
<li><a href="http://vk.com/album-48310141_220910040">http://vk.com/album-48310141_220910040</a>

<li><a href="http://vk.com/wall23834580_1581?z=album23834580_220485102">http://vk.com/wall23834580_1581?z=album23834580_220485102</a>

<li><a href="http://vk.com/bikutori?z=album-57656894_220663159">http://vk.com/bikutori?z=album-57656894_220663159</a>

<li><a href="https://vk.com/album-81773056_220348933">https://vk.com/album-81773056_220348933</a>

<li><a href="http://neofusion.livejournal.com/4917.html">http://neofusion.livejournal.com/4917.html</a>

</ul>
Все фотографии взяты из открытых источников и принаждлежат своим владельцам.
</p>

<h2>Поделиться</h2>
<p>Если у вас хватило терпения дочитать до этого места — немедленно поделитесь с друзьями
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style "

addthis:title="А ты идешь на «Негромкий подпольник»?"
addthis:url="http://podpolnik.otaku.ru/"
>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_facebook"></a>
<a class="addthis_button_vk"></a>
<a class="addthis_button_livejournal"></a>
<a class="addthis_button_email"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<!--div id="imagealign">
<img src="img/title2011a.gif" />
</div-->

<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e2727a27fdc7c89"></script>
<!-- AddThis Button END -->
</p>

</div>
</div>
