<div id="announce">
<div id="content">
<h1>Негромкий подпольник 2014</h1>
<p>30 августа 2014 года, Воронеж, 9 километр, лес. Сбор на остановке &laquo;Студгородок ВПИ&raquo; (9-й километр)
<!--Вы можете добавить это событие себе 
<a href="https://www.google.com/calendar/event?action=TEMPLATE&text=Негромкий+подпольник+2014&dates=20140830T060000Z/20140830T150000Z&details=http://podpolnik.otaku.ru/">в календарь</a>,
или присоединиться к нему <a  target="_blank" href="http://vk.com/podpolnik2014">ВКонтакте</a>. -->
</p>
</p>
<!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту  (начало) -->
<script src="http://api-maps.yandex.ru/1.1/?key=AB9zKk4BAAAADSebRgMA7OjJ_GAAJuBjARiSoW8_3A4G3KcAAAAAAAAAAAAo5pjL-FkXLvPLMNQEJ2Lt_dOPEA==&modules=pmap&wizard=constructor" type="text/javascript"></script>
<script type="text/javascript">
    YMaps.jQuery(window).load(function () {
        var map = new YMaps.Map(YMaps.jQuery("#YMapsID-711")[0]);
        map.setCenter(new YMaps.GeoPoint(39.168197,51.756161), 14, YMaps.MapType.MAP);
        map.addControl(new YMaps.Zoom());
        map.addControl(new YMaps.ToolBar());
        YMaps.MapType.PMAP.getName = function () { return "Народная"; };
        map.addControl(new YMaps.TypeControl([
            YMaps.MapType.MAP,
            YMaps.MapType.SATELLITE,
            YMaps.MapType.HYBRID,
            YMaps.MapType.PMAP
        ], [0, 1, 2, 3]));

        YMaps.Styles.add("constructor#pmlbmPlacemark", {
            iconStyle : {
                href : "http://api-maps.yandex.ru/i/0.3/placemarks/pmlbm.png",
                size : new YMaps.Point(28,29),
                offset: new YMaps.Point(-8,-27)
            }
        });

       map.addOverlay(createObject("Placemark", new YMaps.GeoPoint(39.151289,51.752991), "constructor#pmlbmPlacemark", "Негромкий подпольник 2014"));
       map.addOverlay(createObject("Placemark", new YMaps.GeoPoint(39.18488,51.755628), "constructor#pmlbmPlacemark", "Сюда ходят автобусы"));
        
        function createObject (type, point, style, description) {
            var allowObjects = ["Placemark", "Polyline", "Polygon"],
                index = YMaps.jQuery.inArray( type, allowObjects),
                constructor = allowObjects[(index == -1) ? 0 : index];
                description = description || "";
            
            var object = new YMaps[constructor](point, {style: style, hasBalloon : !!description});
            object.description = description;
            
            return object;
        }
    });
</script>
<p>

<div id="YMapsID-711" style="width:640px;height:480px"></div>
</p>
<!--div style="width:640px;text-align:right;font-family:Arial"><a href="http://api.yandex.ru/maps/tools/constructor/" style="color:#1A3DC1">Создано с помощью инструментов Яндекс.Карт</a></div-->
<!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (конец) -->
<p><a href="/">Назад</a></p>
</div>
</div>