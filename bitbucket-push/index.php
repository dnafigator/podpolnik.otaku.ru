<?php
//
if (!function_exists('getallheaders')) 
{ 
    function getallheaders() 
    { 
       $headers = array (); 
       foreach ($_SERVER as $name => $value) 
       { 
           if (substr($name, 0, 5) == 'HTTP_') 
           { 
               $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value; 
           } 
       } 
       return $headers; 
    } 
} 

//echo "=================================Headers=================================\n";
$headers = getallheaders();
//var_dump($headers);
$event = @$headers['X-Event-Key'];
$uid = @$headers['X-Hook-Uuid'];

echo "\nEvent: $event\nUID: $uid\n";
$hookUID = require_once('config.php');
if (('repo:push' == $event) && ($hookUID == $uid)){
    $body = file_get_contents('php://input');
    //echo "=================================Body=================================\n$body\n";
    if ($body) {
        if ($bodyJson = json_decode($body, false)) {
            //echo "=================================Json=================================\n";
            //var_dump($bodyJson);
            //echo "\n";
            $master = false;
            foreach ($bodyJson->push->changes as $change) {
                if ('branch' == $change->new->type && 'master' == $change->new->name ) {
                    echo "MASTER push found\n";
                    $master = true;
                    break; 
                }
            }
            if ($master) {
                echo "Pulling\n";
                echo shell_exec('git pull 2>&1 3>&1');
                die;
            }
        }
    }
}
echo 'some error occured';
