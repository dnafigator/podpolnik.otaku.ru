<div id="logo">
<div id="logotext">
	<img src="/img/logo2011b.jpg" />

<div id="titlesign">
<p>Дорогие друзья!<br/>
Мы рады представить вам самое тихое мероприятие года</p>
<h1>Негромкий подпольник 2011</h1>
</div>
</div>
</div>

<div id="announce">
<div id="content">
<p>
<b>Обратите внимание! Дата &laquo;Негромкого подпольника&raquo; перенесена на 27 августа.</b>
</p>
<p>Знакомая ситуация:
<ul>
<li>вы прекрасно поете дома, а в микрофон все выходит как-то не так;</li>
<li>вы играли, как бог, а звукорежиссер все испортил;</li>
<li>а может, вы вообще предпочитаете живой голос шипящей фонограмме?</li>
</ul>
Тогда &laquo;Негромкий подпольник&raquo; ждет вас, а прогресс идет мимо!</p>

<h2>Как</h2>
<p>Вот в таких условиях вам придется работать:
<ul>
<li>никаких микрофонов;</li>
<li>звуковое сопровождение — живая музыка или трек соответствующей громкости;</li>
<li>в сценках — никаких записанных голосов;</li>
<li>и даже спецэффекты настоятельно рекомендуется производить в реальном времени.</li>
</ul>
Спойте нам песенку под гитару, заразите всех искрометным юмором или поразите глубиной развернувшейся драмы.</p>


<h2>Кому</h2>
<p>Музыкантам — попробуйте себя в абсолютной акустике.<br/>
Косплеерам — выучите слова и освободите руки от микрофонов.<br/>
Караокерам — мы так любим слушать вас вживую в гримерках! Колонки и плеер — и ваш тихий, но звездный час настал.<br/>
Зрителям — СИДИТЕ ТИХО! А вот аплодировать нужно громко.</p>
<h2>Где и когда</h2>
<p>

<a href="?y=map.tpl">В лесу недалеко от Воронежа</a> 27 августа 2011 (суббота)
в 14:00 — общий сбор на остановке &laquo;Студгородок ВГПУ&raquo; (9-й километр).
Вы даже можете добавить это событие себе <a href="http://www.google.com/calendar/event?action=TEMPLATE&text=%D0%9D%D0%B5%D0%B3%D1%80%D0%BE%D0%BC%D0%BA%D0%B8%D0%B9%20%D0%BF%D0%BE%D0%B4%D0%BF%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%202011&dates=20110827T100000Z/20110827T190000Z&details=&location=%D0%92%D0%BE%D1%80%D0%BE%D0%BD%D0%B5%D0%B6%2C%209%20%D0%BA%D0%B8%D0%BB%D0%BE%D0%BC%D0%B5%D1%82%D1%80%2C%20%D0%BB%D0%B5%D1%81&trp=true&sprop=http%3A%2F%2Fpodpolnik.otaku.ru%2F&sprop=name:%D0%9D%D0%B5%D0%B3%D1%80%D0%BE%D0%BC%D0%BA%D0%B8%D0%B9%20%D0%BF%D0%BE%D0%B4%D0%BF%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%202011" target="_blank">в&nbsp;календарь</a>, или присоединиться к нему <a target="_blank" href="http://vkontakte.ru/event28902966">ВКонтакте</a>.</p>

<h2>Для желающих участвовать</h2>
<p>Подайте <a href="/app">заявку</a>!</p>

<h2>Связаться с организаторами</h2>
<p>Отправляйте ваши идеи, мысли, вопросы и даже претензии на <a href="mailto:podpolnik@otaku.ru">podpolnik@otaku.ru</a><br/></p>


<h2>Поделиться</h2>
<p>Если у вас хватило терпения дочитать до этого места — немедленно поделитесь с друзьями!
<div id="imagealign">
<img src="img/title2011a.gif" />
</div>
<!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style "

addthis:title="А ты идешь на «Негромкий подпольник»?"
addthis:url="http://podpolnik.otaku.ru/"
>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_facebook"></a>
<a class="addthis_button_vk"></a>
<a class="addthis_button_livejournal"></a>
<a class="addthis_button_email"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_clickback":true};</script>
<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4e2727a27fdc7c89"></script>
<!-- AddThis Button END -->
</p>
</div>
</div>
