<div id="announce">
<div id="content">
<h1>Негромкий подпольник 2012</h1>
<p>18 августа 2012 года, Воронеж, 9 километр, лес. Сбор на остановке &laquo;Студгородок ВПИ&raquo; (9-й километр)
<!--Вы можете добавить это событие себе 
<a href="http://www.google.com/calendar/event?action=TEMPLATE&text=%D0%9D%D0%B5%D0%B3%D1%80%D0%BE%D0%BC%D0%BA%D0%B8%D0%B9%20%D0%BF%D0%BE%D0%B4%D0%BF%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%202012&dates=20110827T100000Z/20110827T190000Z&details=&location=%D0%92%D0%BE%D1%80%D0%BE%D0%BD%D0%B5%D0%B6%2C%209%20%D0%BA%D0%B8%D0%BB%D0%BE%D0%BC%D0%B5%D1%82%D1%80%2C%20%D0%BB%D0%B5%D1%81&trp=true&sprop=http%3A%2F%2Fpodpolnik.otaku.ru%2F&sprop=name:%D0%9D%D0%B5%D0%B3%D1%80%D0%BE%D0%BC%D0%BA%D0%B8%D0%B9%20%D0%BF%D0%BE%D0%B4%D0%BF%D0%BE%D0%BB%D1%8C%D0%BD%D0%B8%D0%BA%202011" target="_blank">в календарь</a>,
или присоединиться к нему <a  target="_blank" href="http://vkontakte.ru/event28902966">ВКонтакте</a>. -->
</p>
</p>
<!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту  (начало) -->
<script src="http://api-maps.yandex.ru/1.1/?key=AB9zKk4BAAAADSebRgMA7OjJ_GAAJuBjARiSoW8_3A4G3KcAAAAAAAAAAAAo5pjL-FkXLvPLMNQEJ2Lt_dOPEA==&modules=pmap&wizard=constructor" type="text/javascript"></script>
<script type="text/javascript">
    YMaps.jQuery(window).load(function () {
        var map = new YMaps.Map(YMaps.jQuery("#YMapsID-711")[0]);
        map.setCenter(new YMaps.GeoPoint(39.168197,51.756161), 14, YMaps.MapType.MAP);
        map.addControl(new YMaps.Zoom());
        map.addControl(new YMaps.ToolBar());
        YMaps.MapType.PMAP.getName = function () { return "Народная"; };
        map.addControl(new YMaps.TypeControl([
            YMaps.MapType.MAP,
            YMaps.MapType.SATELLITE,
            YMaps.MapType.HYBRID,
            YMaps.MapType.PMAP
        ], [0, 1, 2, 3]));

        YMaps.Styles.add("constructor#pmlbmPlacemark", {
            iconStyle : {
                href : "http://api-maps.yandex.ru/i/0.3/placemarks/pmlbm.png",
                size : new YMaps.Point(28,29),
                offset: new YMaps.Point(-8,-27)
            }
        });

       map.addOverlay(createObject("Placemark", new YMaps.GeoPoint(39.151289,51.752991), "constructor#pmlbmPlacemark", "Негромкий подпольник 2012"));
       map.addOverlay(createObject("Placemark", new YMaps.GeoPoint(39.18488,51.755628), "constructor#pmlbmPlacemark", "Сюда ходят автобусы"));
        
        function createObject (type, point, style, description) {
            var allowObjects = ["Placemark", "Polyline", "Polygon"],
                index = YMaps.jQuery.inArray( type, allowObjects),
                constructor = allowObjects[(index == -1) ? 0 : index];
                description = description || "";
            
            var object = new YMaps[constructor](point, {style: style, hasBalloon : !!description});
            object.description = description;
            
            return object;
        }
    });
</script>
<p>

<div id="YMapsID-711" style="width:640px;height:480px"></div>
</p>
<!--div style="width:640px;text-align:right;font-family:Arial"><a href="http://api.yandex.ru/maps/tools/constructor/" style="color:#1A3DC1">Создано с помощью инструментов Яндекс.Карт</a></div-->
<!-- Этот блок кода нужно вставить в ту часть страницы, где вы хотите разместить карту (конец) -->
<p><a href="/">Назад</a></p>
</div>
</div>